%*******************************************selectRoute()***********************************************************************************


function [route,cN,cE,cS,cW,outputvid,nframes,outputrate,emergencydir] =  selectRoute(data1,data2,data3,data4,pre)
funList = @VideoAnalyzer;
dataList = {data1,data2,data3,data4}; %# or pass file names 
outPutFileList = {'north.avi','east.avi','south.avi','west.avi'};
nframes={0,0,0,0};
outputcount={0,0,0,0};
outputvid={0,0,0,0};
outputrate={0,0,0,0};
isEmergency={0,0,0,0};
% --- using matlab pool invoking  countCar function in four different directions parallely 
parfor i=1:4
    %# call the function
    [outputcount{i},outputvid{i},outputrate{i},nframes{i},isEmergency{i}]=funList(dataList{i},outPutFileList{i});
end
maxc = 0;
index=-1;
emergencydir=0;
count1=outputcount{1}+outputcount{3};
count2=outputcount{2}+outputcount{4};
flagem=0;
for i=1:4
    if(isEmergency{i} ==1)
        index=-1;
        emergencydir=i;
        flagem=1;     
    end
end
  disp(class(pre==1));
if(flagem==0)
    if(count1 > count2 )
        diff=count1-count2;
        if(pre == 1 && diff <= 3)
          if(outputcount{2}>=outputcount{4})
              index=2;
          else
              index=4;
          end
          else
        if(outputcount{1} >= outputcount{3})
            
        index = 1;
        else
            index = 3;
        end;
        end
    elseif(count1<count2)
        diff=count2-count1;
        if(pre==2 && diff<=3)
          if(outputcount{1}>=outputcount{3})
              index=1;
          else
              index=3;
          end
        else
    if(outputcount{2} >= outputcount{4})
        index = 2;
    else
        index = 4;
     end;
        end
    else
        if(pre ~=0)
        if(pre ==1 || pre==3)
        if(outputcount{2}>=outputcount{4})
            index=2;
        else
            index=4;
        end
        else
        if(outputcount{1} >= outputcount{3})
        index = 1;
        else
            index = 3;
        end;
        end;
        else
            max=outputcount{1};
            index=1;
            for i=2:4
                if(outputcount{i}>max)
                    max=outputcount{i};
                    index=i;
                end
            end
    end;
    end;
    
    end

cN = outputcount{1};
cS = outputcount{3};
cW = outputcount{4};
cE = outputcount{2};

route =  index;
end
%*******************************************END:selectRoute()***********************************************************************************
