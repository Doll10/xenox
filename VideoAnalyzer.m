
%***********************VideoAnalyzer()*************************

function [cCar,tagg,rate,nframes,isSpecial] = VideoAnalyzer(path,outputpath)

[cCar,tagg,rate,nframes,isSpecial]=
processVideo(path,outputpath);

end;

%**********************END:VideoAnalyzer()***************************************************************************************



%********************** processVideo()********************************************************************************************

function [cCar,tagg,rate,nframes,isSpecial]=
processVideo(path,outputpath)

% --- Reading the media file from the disk --
trafficObj = VideoReader(path);
%get(trafficObj);

nframes = get(trafficObj, 'NumberOfFrames');
darkCarValue = 80;
darkCar = rgb2gray(read(trafficObj,50));
 noDarkCar = imextendedmax(darkCar, darkCarValue);
sedisk = strel('disk',2);
noSmallStructures = imopen(noDarkCar, sedisk);
I = read(trafficObj, 1);
taggedCars = zeros([size(I,1) size(I,2) 3 nframes], class(I));
darkCarValue = 80;
count = 0;
firstFrame=I;
[row1,col1]=size(firstFrame);
row = size(I,1);
col = size(I,2);



%********************isSpecial()*****************************************************************************************



%Case 1:checking whether any VIP car is present or not..



%Case 2: checking whether the video is hazy or corrupted ... 

totalPixel=(row-40)*(col-40);
totalBlack=0;
for i=20:row-20
    for j=20:col-20
        if(I(i,j,1)<5 &&I(i,j,2)<5 && I(i,j,3)<5)
            totalBlack=totalBlack+1;
        end
    end
end
if((totalBlack/totalPixel)*100 >=70)
    isSpecial=1;
    cCar=0;
    tagg=taggedCars;
    rate= get(trafficObj,'FrameRate');
else
    isSpecial=0;

%*******************END:isSpecial()*******************************************************************************



index=row1-35;
k=1;
frameRate = get(trafficObj,'FrameRate');
rate=round(mod(frameRate,10));
if(rate == 0)
    rate=8;
end;
x=1;
y=1;



while( k < nframes)
    singleFrame = read(trafficObj,k);
    
    I = rgb2gray(singleFrame);
   % Removing the background noise from the current frame 
    noDarkCars = imextendedmax(I, darkCarValue); 
    % Removing smaller objects from the current frame
    noSmallStructures = imopen(noDarkCars, sedisk);

    noSmallStructures = bwareaopen(noSmallStructures, 150);
     
    taggedCars(:,:,:,k) = singleFrame;
    
    
    
  % finding the centroid and area of the maximum object in the current frame 
   stats = regionprops(noSmallStructures, {'Centroid','Area'});    
    if ~isempty([stats.Area])
        % if any object is present in the frame then rectangle is drawn at the centroid of the object 
        areaArray = [stats.Area];
        [junk,idx] = max(areaArray);
        c = stats(idx).Centroid;
        
        c = floor(fliplr(c));
        width = 4;
        first = c(1) - width;
        if(first == 0)
            first = 1;
        end;
        
        row = first:c(1)+width;
        col = c(2)- width:c(2)+width;
        

  %******************countNoOfVehicles()******************************************************************************************************** 

  % -- when the car comes within a particuler region it is counted as car
        if(c(1) >= (index-8) && c(1) <= (index+8) )
         
            if(noSmallStructures(x,y) ~=255)
                
        x=c(1);
        y=c(2);
        count=count+1;
      
       for h=k+1:k+(rate)
           if(h > nframes)
               break;
           end;
          singleFrame = read(trafficObj, h);
          taggedCars(:,:,:,h) = singleFrame;
         
     
          
       end;
        k=k+(rate-1);
          
     
        end;
        end;
        taggedCars(row,col,1,k) = 255;
        taggedCars(row,col,2,k) = 0;
        taggedCars(row,col,3,k) = 0;
     
    end
    

 %*****************END:countNoOfVehicles()*********************************************************************************

   k=k+1;
   
end


frameRate = get(trafficObj,'FrameRate');
implay(taggedCars,frameRate);
tagg=taggedCars;
rate=frameRate;
cCar=count;
end
end
end;

%**********************END:processVideo()********************************************************************************
