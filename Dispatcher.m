function varargout = Dispatcher(varargin)

% DISPATCHER MATLAB code for Dispatcher.fig
%      DISPATCHER, by itself, creates a new DISPATCHER or raises the existing
%      singleton*.
%
%      H = DISPATCHER returns the handle to a new DISPATCHER or the handle to
%      the existing singleton*.
%
%      DISPATCHER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPATCHER.M with the given input arguments.
%
%      DISPATCHER('Property','Value',...) creates a new DISPATCHER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before processvideo_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to processvideo_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help processvideo

% Last Modified by GUIDE v2.5 16-Apr-2016 19:07:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @processvideo_OpeningFcn, ...
                   'gui_OutputFcn',  @processvideo_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT







% --- Executes just before Dispatcher is made visible.
function dispatcher_OpeningFcn(hObject, eventdata, handles, varargin)

% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DISPATCHER (see VARARGIN)

% Choose default command line output for DISPATCHER
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% --- Creating matlab pool ---
myCluster = parcluster('local');
 myCluster.NumWorkers = 4;  % 'Modified' property now TRUE
saveProfile(myCluster);
matlabpool open 4;  

global cycletime;
global cameraOn ;
global arduinoser;
cameraOn = 1;

% Invoking Timer to save the timer value;
[cycletime,port]  = setTimer;
arduinoser=serial(port,'BaudRate',9600);
fopen(arduinoser);
set(handles.timerval,'String',cycletime);
set(handles.timerval,'enable','off');
set(handles.northbt,'enable','off');
set(handles.eastbt,'enable','off');
set(handles.westbt,'enable','off');
set(handles.southbt,'enable','off');
set(handles.viewfootage,'enable','off');
% warning('off','all');


% UIWAIT makes dispatcher wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dispatcher_OutputFcn(hObject, eventdata, handles) 

% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes when user attempts to close figure1.






% --- Executes on button press in west1.
function west_Callback(hObject, eventdata, handles)

% hObject    handle to west1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%resetAllText();
% ---- To take video input for processing in west direction ---
[ip,jp]=uigetfile({'*.mp4;*.mj2;*.avi;'});
global westpath;
westpath=strcat(jp,ip);





% --- Executes on button press in north1.
function north_Callback(hObject, eventdata, handles)


% hObject    handle to north1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% ---- For opening the webcamera for capturing footage ---- 
global cameraOn;
global northpath;
if (cameraOn == 1)
   
Camera();
cameraOn=0;
set(handles.viewfootage,'enable','on');
end;

set(handles.nc,'string',''); 
set(handles.ec,'string',''); 
set(handles.wc,'string',''); 
set(handles.sc,'string','');    
set(handles.route,'string',''); 
set(handles.npr,'string','');
set(handles.spr,'string','');
set(handles.epr,'string','');
set(handles.wpr,'string','');
set(handles.npr,'string','');
set(handles.nrb,'Value',0);
set(handles.srb,'Value',0);
set(handles.erb,'Value',0);
set(handles.wrb,'Value',0);
% ---- To take video input for processing in north direction ---

[ip,jp]=uigetfile({'*.mp4;*.mj2;*.avi;'});
northpath=strcat(jp,ip);





% --- Executes on button press in south1.
function south_Callback(hObject, eventdata, handles)

% hObject    handle to south1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ---- To take video input for processing in south direction ---

[ip,jp]=uigetfile({'*.mp4;*.mj2;*.avi;'});
global southpath;
southpath=strcat(jp,ip);






% --- Executes on button press in east1.
function east_Callback(hObject, eventdata, handles)

% hObject    handle to east1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% ---- To take video input for processing in east direction ---

[ip,jp]=uigetfile({'*.mp4;*.mj2;*.avi;'});
global eastpath;
eastpath=strcat(jp,ip);



% --- Executes on button press in SelectRoute.
function SelectRoute_Callback(hObject, eventdata, handles)

% hObject    handle to SelectRoute (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
  set(handles.nrb,'enable','off');
        set(handles.srb,'enable','off');
        set(handles.erb,'enable','off');
        set(handles.wrb,'enable','off');
        set(handles.westbt,'enable','off');
        set(handles.north,'enable','off');
        set(handles.south,'enable','off');
        set(handles.east,'enable','off');
        set(handles.west,'enable','off');
        set(handles.eastbt,'enable','off');
        set(handles.northbt,'enable','off');
        set(handles.southbt,'enable','off');
        set(handles.resettimer,'enable','off');
        set(handles.viewfootage,'enable','off');
        set(handles.SelectRoute,'enable','off');
        
oldpointer = get(handles.figure1,'pointer');
set(handles.figure1,'pointer','watch')
drawnow;
global northpath;
global southpath;
global eastpath;
global westpath;
global listvid;
global outputrate;
global cycletime;
global nframes;
global pre; 
global route1;

% --- Invoking selectRoute for selecting the route based on the no of vehicles in four direction ---

%*********************sendVideo()**************************
 
[route1,cN,cE,cS,cW,listvid,nframes,outputrate,emergencydir] = selectRoute(northpath, eastpath,southpath,westpath,pre);


%********************END:sendVideo()**********************

 pre=route1;

%***************** triggersignal()*********************

global arduinoser;
fprintf(arduinoser,'%s',char(route1));

%*******************END:triggersignal()*****************

% --- If any emergency case arises then a notification is sent to the traffic guard

%********************setManualControlling()******************


if(route1==-1)
    value=notificationPanel;
    disp(value);
    if(value == 2)
        if(emergencydir == 1)
            str1='North';
        elseif(emergencydir==2)
                str1='East';
        elseif(emergencydir ==3)
                    str1='South';
                else
                    str1='West';
         end
        waitfor(msgbox(strcat('Emergency in ',str1,' direction ',' Please set the priority..')));
        set(handles.nrb,'enable','on');
        set(handles.srb,'enable','on');
        set(handles.erb,'enable','on');
        set(handles.wrb,'enable','on');
        
    end
    
    %**********************END:setManualControlling()**************
    
else
    % --- depending on the result of selectRoute  the selected route is being highlighted
    
 if(route1==1)
     text='North';
     set(handles.nrb,'enable','on');
     set(handles.nrb,'Value',1);        
      set(handles.nrb,'enable','off');
 elseif(route1==2)
    text='East';
     set(handles.erb,'enable','on');
    set(handles.erb,'Value',1);
     set(handles.erb,'enable','off');
 elseif(route1==3)
      set(handles.srb,'enable','on');
     set(handles.srb,'Value',1);
      set(handles.srb,'enable','off');
     text='South';
 else
      set(handles.wrb,'enable','on');
     set(handles.wrb,'Value',1);
      set(handles.wrb,'enable','off');
      text='West';
      
 end;

 set(handles.npr,'enable','on');
set(handles.wpr,'enable','on');
set(handles.spr,'enable','on');
set(handles.epr,'enable','on');

if(route1 ==1 || route1 ==3)
    set(handles.npr,'string','high');
    set(handles.spr,'string','high');
    set(handles.epr,'string','low');
    set(handles.wpr,'string','low');
else
    set(handles.npr,'string','low');
    set(handles.spr,'string','low');
    set(handles.epr,'string','high');
    set(handles.wpr,'string','high');
end;
set(handles.route,'enable','on');
set(handles.route,'string',''); 
set(handles.nc,'enable','on');
set(handles.sc,'enable','on');
set(handles.ec,'enable','on');
set(handles.wc,'enable','on');

set(handles.nc,'string',''); 
set(handles.ec,'string',''); 
set(handles.wc,'string',''); 
set(handles.sc,'string',''); 

 
 
set(handles.route,'string',text);
set(handles.nc,'string',num2str(cN));
set(handles.ec,'string',num2str(cE));
set(handles.sc,'string',num2str(cS));
set(handles.wc,'string',num2str(cW));

% ---- saving the processed video 


aviObject=avifile('north.avi');
for i=1:nframes{1}
    aviObject = addframe(aviObject,listvid{1}(:,:,:,i));         
end
aviObject = close(aviObject);



aviObject=avifile('east.avi');
for i=1:nframes{2}
    aviObject = addframe(aviObject,listvid{2}(:,:,:,i));         
end
aviObject = close(aviObject);



aviObject=avifile('south.avi');
for i=1:nframes{3}
    aviObject = addframe(aviObject,listvid{3}(:,:,:,i));         
end
aviObject = close(aviObject);




aviObject=avifile('west.avi');
for i=1:nframes{4}
    aviObject = addframe(aviObject,listvid{4}(:,:,:,i));         
end
aviObject = close(aviObject);


set(handles.northbt,'enable','on');
set(handles.eastbt,'enable','on');
set(handles.westbt,'enable','on');
set(handles.southbt,'enable','on');

set(handles.resettimer,'enable','on');
set(handles.viewfootage,'enable','on');

% disableing text boxes
set(handles.nc,'enable','off');
set(handles.wc,'enable','off');
set(handles.sc,'enable','off');
set(handles.ec,'enable','off');

set(handles.npr,'enable','off');
set(handles.wpr,'enable','off');
set(handles.spr,'enable','off');
set(handles.epr,'enable','off');
set(handles.route,'enable','off');
 
% if(isvalid(handles.figure1)~=0)
str=strcat('Note : System will request for video after ',cycletime,' seconds');
waitfor(warndlg(str));
  
  
  pause on;
  pause(str2double(cycletime));
  pause off;


%  --- After one cycle time completes then system is again requseting the camera for capturing the video  
  msgbox('One cycle completed... Please provide Input videos!!');
 set(handles.resettimer,'enable','off');
  set(handles.viewfootage,'enable','off');

Camera(); 

set(handles.SelectRoute,'enable','on');
set(handles.westbt,'enable','on');
set(handles.eastbt,'enable','on');
set(handles.northbt,'enable','on');
set(handles.southbt,'enable','on');
end;
set(handles.north,'enable','on');
set(handles.south,'enable','on');
set(handles.west,'enable','on');
set(handles.east,'enable','on');

set(handles.resettimer,'enable','on');
  set(handles.viewfootage,'enable','on');
  set(handles.figure1,'pointer',oldpointer);
  


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)

% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure
delete(hObject);
% closing the matlab pool 
matlabpool close;


global arduinoser;
fclose(arduinoser);



function nc_Callback(hObject, eventdata, handles)

% hObject    handle to nc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of nc as text
%        str2double(get(hObject,'String')) returns contents of nc as a double






% --- Executes during object creation, after setting all properties.
function nc_CreateFcn(hObject, eventdata, handles)

% hObject    handle to nc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function ec_Callback(hObject, eventdata, handles)

% hObject    handle to ec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ec as text
%        str2double(get(hObject,'String')) returns contents of ec as a double





% --- Executes during object creation, after setting all properties.
function ec_CreateFcn(hObject, eventdata, handles)

% hObject    handle to ec (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






function sc_Callback(hObject, eventdata, handles)

% hObject    handle to sc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sc as text
%        str2double(get(hObject,'String')) returns contents of sc as a double





% --- Executes during object creation, after setting all properties.
function sc_CreateFcn(hObject, eventdata, handles)

% hObject    handle to sc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function wc_Callback(hObject, eventdata, handles)

% hObject    handle to wc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wc as text
%        str2double(get(hObject,'String')) returns contents of wc as a double




% --- Executes during object creation, after setting all properties.
function wc_CreateFcn(hObject, eventdata, handles)

% hObject    handle to wc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






function route_Callback(hObject, eventdata, handles)

% hObject    handle to route (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of route as text
%        str2double(get(hObject,'String')) returns contents of route as a double






% --- Executes during object creation, after setting all properties.
function route_CreateFcn(hObject, eventdata, handles)

% hObject    handle to route (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function timerval_Callback(hObject, eventdata, handles)

% hObject    handle to timerval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of timerval as text
%        str2double(get(hObject,'String')) returns contents of timerval as a double







% --- Executes during object creation, after setting all properties.
function timerval_CreateFcn(hObject, eventdata, handles)

% hObject    handle to timerval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes on button press in northbt.
function northbt_Callback(hObject, eventdata, handles)

% hObject    handle to northbt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% -- Invoking a native java program to show the video footage
javaaddpath([pwd '\ShowVideo.jar']);
ShowVideo.show('north.avi');




% --- Executes on button press in eastbt.
function eastbt_Callback(hObject, eventdata, handles)

% hObject    handle to eastbt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% -- Invoking a native java program to show the video footage
javaaddpath([pwd '\ShowVideo.jar']);
ShowVideo.show('east.avi');





% --- Executes on button press in southbt.
function southbt_Callback(hObject, eventdata, handles)

% hObject    handle to southbt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% -- Invoking a native java program to show the video footage
javaaddpath([pwd '\ShowVideo.jar']);
ShowVideo.show('south.avi');




% --- Executes on button press in westbt.
function westbt_Callback(hObject, eventdata, handles)

% hObject    handle to westbt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% -- Invoking a native java program to show the video footage
javaaddpath([pwd '\ShowVideo.jar']);
ShowVideo.show('west.avi');




function npr_Callback(hObject, eventdata, handles)

% hObject    handle to npr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of npr as text
%        str2double(get(hObject,'String')) returns contents of npr as a double


    

% --- Executes during object creation, after setting all properties.
function npr_CreateFcn(hObject, eventdata, handles)

% hObject    handle to npr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function epr_Callback(hObject, eventdata, handles)

% hObject    handle to epr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of epr as text
%        str2double(get(hObject,'String')) returns contents of epr as a double





    
% --- Executes during object creation, after setting all properties.
function epr_CreateFcn(hObject, eventdata, handles)

% hObject    handle to epr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function spr_Callback(hObject, eventdata, handles)

% hObject    handle to spr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of spr as text
%        str2double(get(hObject,'String')) returns contents of spr as a double





% --- Executes during object creation, after setting all properties.
function spr_CreateFcn(hObject, eventdata, handles)

% hObject    handle to spr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function wpr_Callback(hObject, eventdata, handles)

% hObject    handle to wpr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wpr as text
%        str2double(get(hObject,'String')) returns contents of wpr as a double





% --- Executes during object creation, after setting all properties.
function wpr_CreateFcn(hObject, eventdata, handles)

% hObject    handle to wpr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





% --- Executes on button press in nrb.
function nrb_Callback(hObject, eventdata, handles)

% hObject    handle to nrb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of nrb


set(handles.srb,'Value',0);
set(handles.erb,'Value',0);
set(handles.wrb,'Value',0);
fprintf(arduinoser,'%s',char(1));
set(handles.npr,'string','high');
    set(handles.spr,'string','high');
    set(handles.epr,'string','low');
    set(handles.wpr,'string','low');
    set(handles.route,'string','North'); 





% --- Executes on button press in erb.
function erb_Callback(hObject, eventdata, handles)

% hObject    handle to erb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of erb
set(handles.srb,'Value',0);
set(handles.nrb,'Value',0);
set(handles.wrb,'Value',0);
fprintf(arduinoser,'%s',char(2));
set(handles.npr,'string','low');
    set(handles.spr,'string','low');
    set(handles.epr,'string','high');
    set(handles.wpr,'string','high');
    set(handles.route,'string','East'); 

% --- Executes on button press in srb.
function srb_Callback(hObject, eventdata, handles)

% hObject    handle to srb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of srb
set(handles.nrb,'Value',0);
set(handles.erb,'Value',0);
set(handles.wrb,'Value',0);
fprintf(arduinoser,'%s',char(3));
    set(handles.npr,'string','high');
    set(handles.spr,'string','high');
    set(handles.epr,'string','low');
    set(handles.wpr,'string','low');
    set(handles.route,'string','South'); 




% --- Executes on button press in wrb.
function wrb_Callback(hObject, eventdata, handles)

% hObject    handle to wrb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of wrb
set(handles.srb,'Value',0);
set(handles.erb,'Value',0);
set(handles.nrb,'Value',0);
fprintf(arduinoser,'%s',char(4));
    set(handles.npr,'string','low');
    set(handles.spr,'string','low');
    set(handles.epr,'string','high');
    set(handles.wpr,'string','high');
    set(handles.route,'string','West'); 






% --- Executes on button press in resettimer.
function resettimer_Callback(hObject, eventdata, handles)


% hObject    handle to resettimer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --- Invoking the Timer for reseting the cycletime -- 
global cycletime;
set(handles.timerval,'enable','on');
cycletime  = Timer; 
set(handles.timerval,'String',cycletime);
set(handles.timerval,'enable','off');





% --- Executes on button press in viewfootage.
function viewfootage_Callback(hObject, eventdata, handles)

% hObject    handle to viewfootage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% -- Invoking a native java program to show the video footage
javaaddpath([pwd '\ShowVideo.jar']);
ShowVideo.show('output.avi');