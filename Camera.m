%***********************Camera()******************************

function [ output_args ] = Camera()

recordVideo();

end;

%*********************END:Camera()*************************


%*********************recordVideo()***********************

function [output_args]=recordVideo()


%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
% -- capturing video using webcam of the laptop


vid = videoinput('winvideo',1, 'YUY2_320x240');
set(vid, 'FramesPerTrigger', Inf);
set(vid, 'ReturnedColorspace', 'rgb');

vid.FrameGrabInterval = 1;  % distance between captured frames 

start(vid);
global aviObject ;
aviObject=avifile('output.avi');   % Create a new AVI file

i=1;
capturetime=5;
framerate = 30;
numframes = floor(capturetime * framerate);


while(i <=numframes)                  % Capture  frames for 10 sec.
      I=getsnapshot(vid);

  F = im2frame(I);                    % Convert I to a movie frame
  aviObject = addframe(aviObject,F);  % Add the frame to the AVI file
  i=i+1;   
end


%***************destroying the aviObject.....

aviObject = close(aviObject);
stop(vid);
delete(vid);
end
end;

%*******************************END:recordVideo()***************